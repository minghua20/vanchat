import React, { useContext, useState } from "react";
// import Camerai from "../imgs/cam.png"
// import Addfriendi from "../imgs/add.png"
// import Morei from "../imgs/more.png"
import LOGO from "../imgs/logo.png"
import Messages from "./Messages";
import Input from "./Input";
import { ChatContext } from "../context/ChatContext";
import { doc, updateDoc } from "firebase/firestore";
import { db } from "../firebase";
import { AuthContext } from "../context/AuthContext";


const Chat = () => {

    const {currentUser} = useContext(AuthContext);
    const {chatdata} = useContext(ChatContext);
    const {dispatch} = useContext(ChatContext);
    const [showInfo, setShowInfo] = useState(false);
    const [newalias, setNewalias] = useState("");
    const [editmode, setEditmode] = useState(false);


    const showmoreInfo = () => {
        setShowInfo(true);
    };

    window.addEventListener("click", e => {
        const userprofileDialog = document.getElementById("userprofileDialog");
        const ellipsis = document.getElementById("ellipsis");
        if (!userprofileDialog?.contains(e.target) && e.target !== ellipsis) {
            setShowInfo(false);
            setEditmode(false);
            setNewalias(chatdata.user.alias);
        }
    });

    const editAlias = () => {
        setNewalias(chatdata.user.alias);
        setEditmode(true);
    };

    const updateAlias = async () => {
        if (newalias.length > 16) {
            alert("Alias exceeds maximum length.");
            return;
        }

        await updateDoc(doc(db, "userChats", currentUser.uid), {
            [chatdata.chatId + ".userInfo"]: {
                uid: chatdata.user.uid,
                displayName: chatdata.user.displayName,
                photoURL: chatdata.user.photoURL,
                email: chatdata.user.email,
                alias: newalias,
            },
        });

        dispatch({
            type: "CHANGE_USER",
            payload: {
                uid: chatdata.user.uid,
                displayName: chatdata.user.displayName,
                photoURL: chatdata.user.photoURL,
                email: chatdata.user.email,
                alias: newalias,
            },
        });

        setEditmode(false);
    };


    return (
        <>
        {chatdata.user.uid && <div className="chat">
            <div className="chatInfo">
                {(chatdata.user.alias === "") && <span>{chatdata.user?.displayName}</span>}
                {(chatdata.user.alias !== "") && <span>{chatdata.user?.alias}</span>}
                <div className="chatIcons">
                    {/* <img src={Camerai} alt="" /> */}
                    {/* <img src={Addfriendi} alt="" /> */}
                    {/* <img src={Morei} alt="" /> */}
                    <i className="fa-solid fa-ellipsis" id="ellipsis" onClick={() => showmoreInfo()}></i>
                </div>
                <dialog className={`userprofile ${showInfo && "onshow"}`} id="userprofileDialog">
                    <div className="basicinfo">
                        <img src={chatdata.user.photoURL} alt="" />
                        <div className="usernameandemail">
                            <span className="username">{chatdata.user.displayName}</span>
                            <span className="email">{chatdata.user.email}</span>
                        </div>
                    </div>
                    <div className="alias">
                        <span>Alias:</span>
                        {!editmode && (chatdata.user.alias !== "") && <span>{chatdata.user.alias}</span>}
                        {!editmode && (chatdata.user.alias === "") && <span className="noalias">Click to add note</span>}
                        {editmode && <input type="text" onChange={e=>setNewalias(e.target.value)} value={newalias} />}
                        <i className={`fa-solid fa-pencil editpen ${!editmode && "show"}`} onClick={() => editAlias()}></i>
                        {editmode && <i className="fa-solid fa-check" onClick={() => updateAlias()}></i>}
                    </div>
                </dialog>
            </div>
            <Messages />
            <Input />
        </div>}
        {(chatdata.user.uid === undefined) && <div className="chat">
            <div className="chatInfo">
                {/* <span></span>
                <div className="chatIcons">
                    <img src={Camerai} alt="" />
                    <img src={Addfriendi} alt="" />
                    <img src={Morei} alt="" />
                </div> */}
            </div>
            <div className="vanchatlogo">
                <img src={LOGO} alt="" />
                <div className="logotext">
                    <i className="fa-solid fa-v"></i>
                    <i className="fa-solid fa-a"></i>
                    <i className="fa-solid fa-n"></i>
                    <i className="fa-solid fa-c"></i>
                    <i className="fa-solid fa-h"></i>
                    <i className="fa-solid fa-a"></i>
                    <i className="fa-solid fa-t"></i>
                </div>
            </div>
        </div>}
        </>
    )
}

export default Chat;
