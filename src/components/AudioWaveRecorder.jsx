import React, { useState } from "react";
import { useEffect } from "react";


const AudioWaveRecorder = ({audiosrc, setAudiosrc, setAudioBlob}) => {

    const [isRecording, setIsRecording] = useState(false);
    const [audioRecorder, setAudioRecorder] = useState(null);
    const [mediastream, setMediastream] = useState(null);

    // Set Timer
    const [seconds, setSeconds] = useState(0);
    const [minutes, setMinutes] = useState(0);
    const [timerstate, setTimerstate] = useState(false);

    useEffect(() => {
        let interval = null;
        if (timerstate) {
            interval = setInterval(() => {
                if (seconds === 59) {
                    setSeconds(0);
                    setMinutes(minutes + 1);
                }
                else
                    setSeconds(seconds + 1);
            }, 1000);
        }
        else if (!timerstate) {
            if (interval)
                clearInterval(interval);
            setSeconds(0);
            setMinutes(0);
        }
        return () => clearInterval(interval);
    }, [timerstate, seconds, minutes]);


    // visualiser setup - create web audio api context and canvas
    const canvas = document.querySelector('.visualizer');
    var canvasCtx = canvas?.getContext("2d");

    //main block for doing the audio recording
    const startRecord = () => {
        setIsRecording(true);
        setTimerstate(true);

        const constraints = { audio: true };
        let chunks = [];

        navigator.mediaDevices.getUserMedia(constraints).then((stream) => {
            setMediastream(stream);

            const mediaRecorder = new MediaRecorder(stream);
            setAudioRecorder(mediaRecorder);

            visualize(stream);

            mediaRecorder.onstop = function(e) {
                const blob = new Blob(chunks, { 'type' : 'audio/ogg; codecs=opus' });
                chunks = [];
                setAudioBlob(blob);
                const audioURL = window.URL.createObjectURL(blob);
                setAudiosrc(audioURL);
                // console.log("audio URL generated");
            }

            mediaRecorder.ondataavailable = function(e) {
                chunks.push(e.data);
            }

            mediaRecorder.start();
        }).catch((err) => {
            alert("Please give permission for the microphone to record audio.");
            setIsRecording(false);
            setTimerstate(false);
        });
    }

    const stopRecord = () => {
        audioRecorder.stop();
        setIsRecording(false);
        setTimerstate(false);
        setAudioRecorder(null);
        // console.log("recorder stopped");
        mediastream.getTracks().forEach(track => {
            track.stop();
            track.enabled = false;
        });
        // const audioContext = new AudioContext()
        // const microphone = audioContext.createMediaStreamSource(mediastream)
        // microphone.disconnect();
        // audioContext.close();
    }

    useEffect(() => {
        if (minutes === 2) {
            stopRecord();
            setTimerstate(false);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [seconds, minutes]);

    function visualize(stream) {
        let audioCtx;
        if(!audioCtx) {
            audioCtx = new AudioContext();
        }

        const source = audioCtx.createMediaStreamSource(stream);

        const analyser = audioCtx.createAnalyser();
        analyser.fftSize = 2048;
        const bufferLength = analyser.frequencyBinCount;
        const dataArray = new Uint8Array(bufferLength);

        source.connect(analyser);

        function draw() {
            const WIDTH = canvas?.width
            const HEIGHT = canvas?.height;
            // let sliceWidth = WIDTH * 1.0 / bufferLength;

            requestAnimationFrame(draw);

            analyser.getByteTimeDomainData(dataArray);

            if (canvasCtx) {
                canvasCtx.fillStyle = "#95EC69";
                canvasCtx.fillRect(0, 0, WIDTH, HEIGHT);

                canvasCtx.beginPath();

                canvasCtx.fillStyle = "#658C49";
            
                let x = 3;

                for(let i = 0; i < 30; i++) {

                    let y;
                    let v = dataArray[i*34] / 128.0;
  
                    if (v < 0.75)
                        y = v * HEIGHT*0.25;
                    else if (v >= 0.75 && v < 0.85)
                        y = v * HEIGHT*0.266;
                    else if (v >= 0.85 && v < 0.98)
                        y = v * HEIGHT*0.283;
                    else if (v >= 0.98 && v < 1.02)
                        y = v * HEIGHT*0.3;
                    else if (v >= 1.02 && v < 1.15)
                        y = v * HEIGHT*0.366;
                    else if (v >= 1.15 && v < 1.25)
                        y = v * HEIGHT*0.433;
                    else
                        y = v * HEIGHT*0.5;

                    canvasCtx.moveTo(x-3, HEIGHT/2-y/2);
                    // canvasCtx.arc(x, HEIGHT/2-y/2, 3, 0, Math.PI, true);
                    canvasCtx.bezierCurveTo(x-1, HEIGHT/2-y/2-10, x+1, HEIGHT/2-y/2-10, x+3, HEIGHT/2-y/2);
                    canvasCtx.fill();

                    canvasCtx.moveTo(x+3, HEIGHT/2-y/2);
                    canvasCtx.lineTo(x+3, HEIGHT/2+y/2);
                    canvasCtx.lineTo(x-3, HEIGHT/2+y/2);
                    canvasCtx.lineTo(x-3, HEIGHT/2-y/2);
                    canvasCtx.fill();

                    canvasCtx.moveTo(x+3, HEIGHT/2+y/2);
                    // canvasCtx.arc(x, HEIGHT/2+y/2, 3, 0, Math.PI, false);
                    canvasCtx.bezierCurveTo(x+1, HEIGHT/2+y/2+10, x-1, HEIGHT/2+y/2+10, x-3, HEIGHT/2+y/2);
                    canvasCtx.fill();

                    x += 10;
                }
            }
        }
        draw();
    };

    const deleteAudio = () => {
        setAudiosrc(null);
        setAudioBlob(null);
    };


    return (
        <div className="AudioRecorder">
            {!isRecording && <button className="record" onClick={() => startRecord()}>
                <i className="fa-regular fa-circle-play"></i>
            </button>}
            {isRecording && <button className="stop" onClick={() => stopRecord()}>
                <i className="fa-regular fa-circle-stop"></i>
            </button>}
            <div className="audiowave">
                {!isRecording && (audiosrc === null) && <span>Click <i className="fa-regular fa-circle-play"></i> to record your voice message</span>}
                <canvas className={`visualizer ${isRecording && "show"}`} ></canvas>
                {isRecording && <span className="timer">{minutes}:{(seconds < 10) && "0"}{seconds}</span>}
                {audiosrc && !isRecording && <audio controls src={audiosrc} />}
            </div>
            <i className={`deleteicon fa-regular fa-trash-can ${audiosrc && !isRecording && "show"}`} onClick={() => deleteAudio()}></i>
        </div>
    )
}

export default AudioWaveRecorder;
