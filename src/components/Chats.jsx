import { doc, onSnapshot, updateDoc } from "firebase/firestore";
import React from "react";
import { useContext } from "react";
import { useEffect } from "react";
import { useState } from "react";
import { AuthContext } from "../context/AuthContext";
import { ChatContext } from "../context/ChatContext";
import { db } from "../firebase";


const getlastmTime = (messagetime) => {
    if (messagetime === undefined)
        return "";
    
    var displayTime;
    var currentTime = new Date();
    if (messagetime.Year !== currentTime.getFullYear()) {
        displayTime = messagetime.Year + '-' + messagetime.Month + '-' + messagetime.Date;
    }
    else {
        if (messagetime.Month !== currentTime.getMonth()+1) {
            displayTime = messagetime.Month + '-' + messagetime.Date;
        }
        else {
            if (messagetime.Date !== currentTime.getDate()) {
                displayTime = messagetime.Month + '-' + messagetime.Date;
            }
            else {
                displayTime = messagetime.Hour + ":" + messagetime.Minute;
            }
        }
    }

    return displayTime;
}

const Chats = ({searchlength, newfriendslength}) => {

    const [chats, setChats] = useState([]);
    const {currentUser} = useContext(AuthContext);
    const {dispatch} = useContext(ChatContext);
    const {chatdata} = useContext(ChatContext);


    useEffect(() => {
        const getChats = () => {
            const unsub = onSnapshot(doc(db, "userChats", currentUser.uid), (doc) => {
                setChats(doc.data())
            });

            return () => {
                unsub();
            }
        };

        currentUser.uid && getChats();
    }, [currentUser.uid]);

    useEffect(() => {
        const updateunread = async () => {
            if (chatdata.user.uid !== undefined) {
                await updateDoc(doc(db, "userChats", currentUser.uid), {
                    // [data.chatId + ".userInfo"]: {
                    //     uid: data.user.uid,
                    //     displayName: data.user.displayName,
                    //     photoURL: data.user.photoURL,
                    //     email: data.user.email,
                    //     alias: data.user.alias,
                    // },
                    [chatdata.chatId + ".unread"]: 0,
                });
            }
        }

        updateunread();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [chatdata.user.uid, chatdata.chatId, currentUser.uid]);

    const handleSelect = (user) => {
        dispatch({
            type: "CHANGE_USER",
            payload: user
        });
    };

    useEffect(() => {
        var checknewfriendlength;
        if (newfriendslength === 0)
            checknewfriendlength = 1;
        else
            checknewfriendlength = 0;
        const chatscollection = document.getElementById("chatscollection");
        chatscollection.style.height = `calc(100% - 70px - 77px - 3px + ${checknewfriendlength}*30px - ${searchlength}*70px - ${newfriendslength}*75px)`;
    }, [searchlength, newfriendslength]);


    return (
        <div id="chatscollection" className="chats">
            {Object.entries(chats)?.sort((a, b) => b[1].date - a[1].date).map((chat) => {
                var unreadm = chat[1].unread;
                if (unreadm > 99)
                    unreadm = "";

                return (
                    <div className={`userChat ${chatdata.user.uid === chat[1].userInfo?.uid && "userselect"}`} key={chat[0]} onClick={() => {
                        handleSelect(chat[1].userInfo);
                    }}>
                        <div className="userphoto">
                            <img src={chat[1].userInfo?.photoURL} alt="" />
                            {(chat[1].unread !== 0) && <div className="mreddot">
                                <span className="unreadmessage">{unreadm}</span>
                            </div>}
                        </div>
                        <div className="userChatInfo">
                            <div className="userandtime">
                                {(chat[1].userInfo?.alias === "") && <span className="username">{chat[1].userInfo?.displayName}</span>}
                                {(chat[1].userInfo?.alias !== "") && <span className="username">{chat[1].userInfo?.alias}</span>}
                                <span className="lastmtime">{getlastmTime(chat[1].displayTime)}</span>
                            </div>
                            <p>{chat[1].lastMessage?.text}</p>
                        </div>
                    </div>
                )
            })}
        </div>
    )
}

export default Chats;
