import React from "react";
import { useState } from "react";
import Chats from "./Chats";
import Navbar from "./Navbar";
import NewfriendsApplication from "./NewfriendsApplication";
import Search from "./Search";

const Sidebar = () => {

    const [searchlength, setSearchlength] = useState(0);
    const [newfriendslength, setNewfriendslength] = useState(0);

    return (
        <div className="sidebar">
            <Navbar />
            <Search setSearchlength={setSearchlength} />
            <NewfriendsApplication newfriendslength={newfriendslength} setNewfriendslength={setNewfriendslength} />
            <Chats searchlength={searchlength} newfriendslength={newfriendslength} />
        </div>
    )
}

export default Sidebar;
