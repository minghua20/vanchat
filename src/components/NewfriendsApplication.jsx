import { doc, onSnapshot } from "firebase/firestore";
import React from "react";
import { useContext } from "react";
import { useEffect } from "react";
import { useState } from "react";
import { AuthContext } from "../context/AuthContext";
import { db } from "../firebase";
import Newfriendinfo from "./Newfriendinfo";


const NewfriendsApplication = ({newfriendslength, setNewfriendslength}) => {

    const [chats, setChats] = useState([]);
    const {currentUser} = useContext(AuthContext);


    useEffect(() => {
        const getChats = () => {
            const unsub = onSnapshot(doc(db, "newfriendsApplication", currentUser.uid), (doc) => {
                setChats(doc.data())
                if (Object.entries(doc.data()).length > 2)
                    setNewfriendslength(2);
                else
                    setNewfriendslength(Object.entries(doc.data()).length)
            });

            return () => {
                unsub();
            }
        };

        currentUser.uid && getChats();

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [currentUser.uid]);


    return (
        <>
        {(Object.entries(chats).length > 0) && <div className="newfriendsApplication">
            <div className="newfriendstext">New friends:</div>
            <div className="newfriendlist">
                {Object.entries(chats)?.sort((a, b) => b[1].date - a[1].date).map((chat) => {
                    return (
                        <Newfriendinfo chat={chat} newfriendslength={newfriendslength} setNewfriendslength={setNewfriendslength} key={chat[0]} />
                    )
                })}
            </div>
        </div>}
        </>
    )
}

export default NewfriendsApplication;
