import { signOut, updateProfile } from "firebase/auth";
import { getDownloadURL, ref, uploadBytesResumable } from "firebase/storage";
import React from "react";
import { useState } from "react";
import { useContext } from "react";
import { AuthContext } from "../context/AuthContext";
import { ChatContext } from "../context/ChatContext";
import { auth, db, storage } from "../firebase";
import { v4 as uuid } from "uuid";
import { doc, getDoc, updateDoc } from "firebase/firestore";
import { useEffect } from "react";


const Navbar = () => {

    const {currentUser} = useContext(AuthContext);
    const {dispatch} = useContext(ChatContext);
    const [newusername, setNewusername] = useState("");
    const [newAvatar, setNewAvatar] = useState(null);
    const [previewAvatar, setPreviewAvatar] = useState(null);
    const [editmode, setEditmode] = useState(false);
    const [isLoadingimg, setIsLoadingImg] = useState(false);
    const [err, setErr] = useState(false);


    const editprofile = () => {
        setPreviewAvatar(null);
        setEditmode(true);
    };

    const updateUserProfile = async () => {

        if (newAvatar === null && newusername === "") {
            setNewusername("");
            setNewAvatar(null);
            setPreviewAvatar(null);
            setEditmode(false);
            return;
        }

        if (newusername.length > 16) {
            alert("Username exceeds maximum length.");
            return;
        }

        const res = await getDoc(doc(db, "users", currentUser.uid));
        const friendlist = (res.data()).friendlist;

        if (newAvatar !== null && previewAvatar !== null) {
            if (newusername !== "") {
                await updateProfile(currentUser, {
                    displayName: newusername,
                    photoURL: previewAvatar,
                });

                await updateDoc(doc(db, "users", currentUser.uid), {
                    displayName: newusername,
                    photoURL: previewAvatar,
                });

                friendlist.map(async (f) => {
                    const combineduId = currentUser.uid > f ? currentUser.uid+f : f+currentUser.uid;
                    const chati = await getDoc(doc(db, "userChats", f));
                    const chatinfo = Object.entries((chati.data())).filter(c => {return c[0] === combineduId});
                    const alias = (chatinfo[0])[1].userInfo.alias;
                    await updateDoc(doc(db, "userChats", f), {
                        [combineduId+".userInfo"]: {
                            uid: currentUser.uid,
                            displayName: newusername,
                            photoURL: previewAvatar,
                            email: currentUser.email,
                            alias: alias,
                        },
                    });
                });
            }
            else {
                await updateProfile(currentUser, {
                    photoURL: previewAvatar,
                });

                await updateDoc(doc(db, "users", currentUser.uid), {
                    photoURL: previewAvatar,
                });

                friendlist.map(async (f) => {
                    const combineduId = currentUser.uid > f ? currentUser.uid+f : f+currentUser.uid;
                    const chati = await getDoc(doc(db, "userChats", f));
                    const chatinfo = Object.entries((chati.data())).filter(c => {return c[0] === combineduId});
                    const alias = (chatinfo[0])[1].userInfo.alias;
                    await updateDoc(doc(db, "userChats", f), {
                        [combineduId+".userInfo"]: {
                            uid: currentUser.uid,
                            displayName: currentUser.displayName,
                            photoURL: previewAvatar,
                            email: currentUser.email,
                            alias: alias,
                        },
                    })
                });
            }
        }
        else {
            if (newusername !== "") {
                await updateProfile(currentUser, {
                    displayName: newusername,
                });

                await updateDoc(doc(db, "users", currentUser.uid), {
                    displayName: newusername,
                });

                friendlist.map(async (f) => {
                    const combineduId = currentUser.uid > f ? currentUser.uid+f : f+currentUser.uid;
                    const chati = await getDoc(doc(db, "userChats", f));
                    const chatinfo = Object.entries((chati.data())).filter(c => {return c[0] === combineduId});
                    const alias = (chatinfo[0])[1].userInfo.alias;
                    await updateDoc(doc(db, "userChats", f), {
                        [combineduId+".userInfo"]: {
                            uid: currentUser.uid,
                            displayName: newusername,
                            photoURL: currentUser.photoURL,
                            email: currentUser.email,
                            alias: alias,
                        },
                    })
                });
            }
        }
        
        setNewusername("");
        setNewAvatar(null);
        setPreviewAvatar(null);
        setEditmode(false);
    };

    useEffect(() => {
        const uploadAvatar = () => {
            if (newAvatar !== null && newAvatar !== undefined) {
                setIsLoadingImg(true);

                const storageRef = ref(storage, uuid());
                const uploadTask = uploadBytesResumable(storageRef, newAvatar);
    
                // Update User Avater
                uploadTask.on(
                    "state_changed",
                    (snapshot) => {
                        const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                        switch (snapshot.state) {
                            case 'paused':
                            console.log('Upload is paused');
                            break;
                            case 'running':
                            // console.log('Upload is running');
                            break;
                            default:
                            break;
                        }
                        console.log("Upload is " + progress + "% done");
                    },
                    (error) => {
                        setErr(true);
                    }, 
                    () => {
                        getDownloadURL(uploadTask.snapshot.ref).then(async (downloadURL) => {
                            setPreviewAvatar(downloadURL);
                        });
                    }
                );
            }
        };
        uploadAvatar();
    }, [newAvatar]);


    return (
        <div className="navbar">
            <div className="logoandlogout">
                <span className="logo">VanChat</span>
                <button onClick={()=>{
                        signOut(auth);
                        dispatch({
                            type: "LOGOUT",
                        });
                    }}>Logout</button>
            </div>
            <div className="user">
                {!editmode && <img src={currentUser.photoURL} alt="" />}
                {editmode && (previewAvatar === null) && <img src={currentUser.photoURL} alt="" />}
                {editmode && (previewAvatar !== null) && <img src={previewAvatar} alt="" onLoad={() => setIsLoadingImg(false)} />}
                {editmode && <input accept="image/*" className="changeAvatar" type="file" id="file" onChange={e=>setNewAvatar(e.target.files[0])} />}
                    {editmode && <label htmlFor="file">
                        {!isLoadingimg && <i className="fa-solid fa-arrow-up-from-bracket"></i>}
                        {isLoadingimg && <i className="loader fa fa-spinner fa-spin"></i>}
                    </label>}
                {!editmode && <div className="usernameemail">
                    <span className="username">{currentUser.displayName}</span>
                    <span className="email">{currentUser.email}</span>
                </div>}
                {editmode && <input type="text" placeholder={currentUser.displayName} onChange={e=>setNewusername(e.target.value)} value={newusername} />}
                {!editmode && <i className="fa-solid fa-user-pen" onClick={() => editprofile()}></i>}
                {editmode && <i className="fa-solid fa-check" onClick={() => updateUserProfile()}></i>}
                {err && <span className="errmessage">Error!</span>}
            </div>
        </div>
    )
}

export default Navbar;
