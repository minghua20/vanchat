import { doc, onSnapshot, updateDoc } from "firebase/firestore";
import React, { useRef } from "react";
import { useContext } from "react";
import { useEffect } from "react";
import { useState } from "react";
import { AuthContext } from "../context/AuthContext";
import { ChatContext } from "../context/ChatContext";
import { db } from "../firebase";
import Message from "./Message";


const Messages = () => {

    const {currentUser} = useContext(AuthContext);
    const [messages, setMessages] = useState([]);
    const {chatdata} = useContext(ChatContext);
    const [imgcounter, setImgcounter] = useState(0);
    const [videosLoaded, setVideosLoaded] = useState(false);
    const [videoNumTemp, setVideoNumTemp] = useState(0);
    const [prevideosLoaded, setPrevideosLoaded] = useState(false);

    useEffect(() => {

        setImgcounter(0);
        setVideosLoaded(false);
        setVideoNumTemp(0);
        setPrevideosLoaded(false);

        const unSub = onSnapshot(doc(db, "chats", chatdata.chatId), (doc) => {
            doc.exists() && setMessages(doc.data().messages);
        })

        return () => {
            unSub();
        }
    }, [chatdata.chatId]);

    useEffect(() => {
        setVideosLoaded(false);
        var videos = document.getElementsByClassName("videos");
        var audios = document.getElementsByClassName("audios");
        var videosNum = videos.length;
        var audiosNum = audios.length;
        var totalNum = videosNum + audiosNum;
        var videosCounter = videoNumTemp;
        if (videoNumTemp !== 0) {
            setPrevideosLoaded(true);
        }
        for (let i = 0; i < videos.length; i++) {
            // eslint-disable-next-line no-loop-func
            videos[i].addEventListener('canplay', (e) => {
                videosCounter++;
                if (videosCounter === totalNum) {
                    setVideosLoaded(true);
                    setVideoNumTemp(videosCounter);
                }
            })
        };
        for (let i = 0; i < audios.length; i++) {
            // eslint-disable-next-line no-loop-func
            audios[i].addEventListener('canplay', (e) => {
                videosCounter++;
                if (videosCounter === totalNum) {
                    setVideosLoaded(true);
                    setVideoNumTemp(videosCounter);
                }
            })
        };
        
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [messages]);

    const ref = useRef();

    useEffect(() => {
        var isLoaded = true;
        var img = document.getElementsByClassName("messageImg");

        for (let i = 0; i < img.length; i++) {
            isLoaded = isLoaded && (img[i].complete && img[i].naturalHeight !== 0);
        }
        
        isLoaded = isLoaded && videosLoaded;

        if (isLoaded && !prevideosLoaded) {
            ref.current?.scrollIntoView();
        }
        if (isLoaded && prevideosLoaded) {
            ref.current?.scrollIntoView({behavior: "smooth"});
        }

    }, [imgcounter, videosLoaded, prevideosLoaded]);

    const updateunread = async () => {
        if (chatdata.user.uid !== undefined) {
            await updateDoc(doc(db, "userChats", currentUser.uid), {
                [chatdata.chatId + ".unread"]: 0,
            });
        }
    }


    return (
        <div className="messages" onClick={() => updateunread()}>
            {messages.map((m, i) => (
                <Message message={m} index={i} premessage={messages[i-1]} length={messages.length} imgcounter={imgcounter} setImgcounter={setImgcounter} key={m.id} />
            )
            )}
            <div ref={ref}></div>
        </div>
    )
}

export default Messages;
