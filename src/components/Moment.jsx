import { arrayUnion, deleteField, doc, getDoc, Timestamp, updateDoc } from 'firebase/firestore';
import React from 'react'
import { useState } from 'react';
import { useContext } from 'react';
import { AuthContext } from '../context/AuthContext';
import { db } from '../firebase';
import data from '@emoji-mart/data';
import Picker from '@emoji-mart/react';


const getDisplayTime = (messagetime) => {
    var displayTime;
    var currentTime = new Date();
    if (messagetime.Year !== currentTime.getFullYear()) {
        displayTime = messagetime.Year + '-' + messagetime.Month + '-' + messagetime.Date + ' ' + messagetime.Hour + ":" + messagetime.Minute;
    }
    else {
        if (messagetime.Month !== currentTime.getMonth()+1) {
            displayTime = messagetime.Month + '-' + messagetime.Date + ' ' + messagetime.Hour + ":" + messagetime.Minute;
        }
        else {
            if (messagetime.Date !== currentTime.getDate()) {
                displayTime = messagetime.Month + '-' + messagetime.Date + ' ' + messagetime.Hour + ":" + messagetime.Minute;
            }
            else {
                displayTime = messagetime.Hour + ":" + messagetime.Minute;
            }
        }
    }

    return displayTime;
}


const Moment = ({moment, index, userchats}) => {

    const {currentUser} = useContext(AuthContext);
    const [interactmode, setInteractmode] = useState(false);
    const [commentmode, setCommentmode] = useState(false);
    const [commenttext, setCommenttext] = useState("");
    const [becommenter, setBecommenter] = useState("");
    const [emojiSelect, setEmojiSelect] = useState(false);
    const [replyuser, setReplyuser] = useState("Comment...");


    window.addEventListener("click", e => {
        const hideicon = document.getElementById("hideicon"+moment[0]);
        if (!hideicon?.contains(e.target))
            setInteractmode(false);

        const commenticon = document.getElementById("commenticon"+moment[0]);
        const likesandcomments = document.getElementById("likesandcomments"+moment[0]);
        if (!commenticon?.contains(e.target) && !likesandcomments?.contains(e.target))
            setCommentmode(false);

        const emojipicker = document.getElementById("emojipicker");
        const emojiicon = document.getElementById("emojiicon");
        if (!emojipicker?.contains(e.target) && e.target !== emojiicon)
            setEmojiSelect(false);
    });

    const imagePopup = (img) => {
        window.open(img, '_blank', 'width=770,height=515,top=145,left=380');
    };

    // const ref = useRef();

    const likethisMoment = async (moment) => {
        await updateDoc(doc(db, "moments", moment[1].momentInfo.uid), {
            [moment[0] + ".likes"]: arrayUnion(currentUser.uid),
        });
        // setInteractmode(false);
        // ref.current?.scrollIntoView({behavior: "smooth"});
    };

    const cancelLike = async (moment) => {
        const res = await getDoc(doc(db, "moments", moment[1].momentInfo.uid));
        const resdata = Object.entries(res.data());
        const momenti = (resdata.filter(r => {return r[0] === moment[0]}))[0];
        const preLikelist = momenti[1].likes;
        let newLikelist = preLikelist.filter(l => {return l !== currentUser.uid});
        await updateDoc(doc(db, "moments", moment[1].momentInfo.uid), {
            [moment[0] + ".likes"]: newLikelist,
        });
        // setInteractmode(false);
        // ref.current?.scrollIntoView({behavior: "smooth"});
    };

    const handleSend = async () => {
        if (commenttext !== "") {
            let checkBlankText = true;
            for (let i = 0; i < commenttext.length; i++) {
                if (commenttext[i] !== " ") {
                    checkBlankText = false;
                    break;
                }
            }

            if (checkBlankText) {
                // alert("Message to be sent cannot be empty!");
                setCommenttext("");
                return;
            }
            else {
                await updateDoc(doc(db, "moments", moment[1].momentInfo.uid), {
                    [moment[0] + ".comments"]: arrayUnion({
                        commenter: currentUser.uid,
                        becommenter: becommenter,
                        content: commenttext,
                        date: Timestamp.now(),
                    }),
                });
                setCommenttext("");
                setBecommenter("");
                setCommentmode(false);
            }
        }
    };

    const replycomment = (userid, useralias) => {
        if (userid !== currentUser.uid) {
            setReplyuser("@ "+useralias);
            setBecommenter(userid);
            setCommentmode(true);
        }
    };

    const deletemoment = async (momentid) => {
        await updateDoc(doc(db, "moments", currentUser.uid), {
            [momentid]: deleteField(),
        });
    };


    var momentusername = "";
    if (moment[1].momentInfo.uid === currentUser.uid)
        momentusername = currentUser.displayName;
    else {
        const combineduId = currentUser.uid > moment[1].momentInfo.uid ? currentUser.uid+moment[1].momentInfo.uid : moment[1].momentInfo.uid+currentUser.uid;
        const chatinfo = (userchats.filter(u => {return u[0] === combineduId}))[0];
        if (chatinfo !== undefined) {
            if (chatinfo[1].userInfo.alias === "")
                momentusername = chatinfo[1].userInfo.displayName;
            else {
                momentusername = chatinfo[1].userInfo.alias;
            }
        }

    }


    const Likelist = moment[1].likes;
    const check = Likelist.filter(l => {return l === currentUser.uid});
    let alreadyLike;
    if (check.length === 0)
        alreadyLike = false;
    else
        alreadyLike = true;


    var displayAlias = [];
    moment[1].likes?.map((l, index) => {
        var userAlias = "";
        if (l === currentUser.uid) {
            userAlias = currentUser.displayName;

            displayAlias.push({
                userid: currentUser.uid,
                useralias: userAlias,
            });
        }
        else {
            const combineduId = currentUser.uid > l ? currentUser.uid+l : l+currentUser.uid;
            const chatinfo = (userchats.filter(u => {return u[0] === combineduId}))[0];
            if (chatinfo !== undefined) {
                if (chatinfo[1].userInfo.alias === "")
                    userAlias = chatinfo[1].userInfo.displayName;
                else {
                    userAlias = chatinfo[1].userInfo.alias;
                }

                displayAlias.push({
                    userid: chatinfo[1].userInfo.uid,
                    useralias: userAlias,
                });
            }
        }

        return 1;
    });


    var checkcomments = false;
    moment[1].comments?.map((c, index) => {
        var visible1 = false;
        if (c.commenter === currentUser.uid)
            visible1 = true;
        else {
            const combineduId1 = currentUser.uid > c.commenter ? currentUser.uid+c.commenter : c.commenter+currentUser.uid;
            var chatinfo = (userchats.filter(u => {return u[0] === combineduId1}))[0];
            if (chatinfo !== undefined) {
                visible1 = true;
            } else {
                visible1 = false;
            }
        }
        var visible2 = true;
        if (c.becommenter !== "") {
            if (c.becommenter === currentUser.uid)
                visible2 = true;
            else {
                const combineduId2 = currentUser.uid > c.becommenter ? currentUser.uid+c.becommenter : c.becommenter+currentUser.uid;
                chatinfo = (userchats.filter(u => {return u[0] === combineduId2}))[0];
                if (chatinfo !== undefined) {
                    visible2 = true;
                } else {
                    visible2 = false;
                }
            }
        }
        checkcomments = checkcomments || (visible1 && visible2);
        return 1;
    });


    return (
            <div className="momentCollection" id={moment[0]}>
                {/* {<div ref={ref}></div>} */}
                <div className="momentUser">
                    <img src={moment[1].momentInfo.photoURL} alt="" />
                </div>
                <div className="momentContent">
                    <p className="username">{momentusername}</p>
                    <span className="momentText">{moment[1].momentInfo.text}</span>
                    {moment[1].momentInfo.picture && <div className={`imgcollection ${(moment[1].momentInfo.picture.length === 1) && "singleimage"} ${(moment[1].momentInfo.picture.length === 4) && "fourimage"}`}>
                        {moment[1].momentInfo.picture?.map((p, i) => {
                            return (
                                <img src={p} alt="" key={`img${i}`} onClick={()=>imagePopup(p)} />
                            )
                        })}
                    </div>}
                    {moment[1].momentInfo.video && <video controls src={moment[1].momentInfo.video} />}
                    {moment[1].momentInfo.audio && <div className='audiobox'>
                        <span>{moment[1].momentInfo.audioname}</span>
                        <audio controls src={moment[1].momentInfo.audio} />
                    </div>}
                    <div className='timeandicons'>
                        <span className="momentTime">{getDisplayTime(moment[1].displaydate)}{(moment[1].momentInfo.uid === currentUser.uid) && <i className="deletemoment fa-regular fa-trash-can" onClick={() => deletemoment(moment[0])}></i>}</span>
                        <div className={`icons ${interactmode && "show"}`}>
                            {!alreadyLike && <div className='likeicon' onClick={() => likethisMoment(moment)}>
                                <i className="fa-regular fa-thumbs-up"></i>
                                <span>Like</span>
                            </div>}
                            {alreadyLike && <div className='likeicon' onClick={() => cancelLike(moment)}>
                                <i className="fa-solid fa-thumbs-up"></i>
                                <span>Cancel</span>
                            </div>}
                            <div className='verticalline'></div>
                            <div className='commenticon' id={`commenticon${moment[0]}`} onClick={() => {
                                setCommentmode(true);
                                setReplyuser("Comment...");
                                setBecommenter("");
                            }}>
                                <i className="fa-regular fa-message"></i>
                                <span>Comment</span>
                            </div>
                        </div>
                        <div className='hideicon' id={`hideicon${moment[0]}`} onClick={() => setInteractmode(true)}>
                            <div className='smallcricle'></div>
                            <div className='smallcricle'></div>
                        </div>
                    </div>
                    {(displayAlias.length !== 0 || commentmode || checkcomments) && <div className='likesandcomments' id={`likesandcomments${moment[0]}`}>
                        {displayAlias.length !== 0 && <div className='likeslist'>
                            <i className="fa-regular fa-thumbs-up"></i>
                            {displayAlias?.map((d, i) => (
                                <span key={d.userid}>
                                    {(i !== 0) && <span>, </span>}
                                    <span className={`useralias ${d.userid === currentUser.uid && "self"}`} onClick={() => replycomment(d.userid, d.useralias)}>{d.useralias}</span>
                                </span>
                            ))}
                        </div>}
                        <div className='commentlist'>
                            {moment[1].comments?.sort((a, b) => a.date - b.date).map((c, i) => {
                                var commenteralias = "";
                                if (c.commenter === currentUser.uid)
                                    commenteralias = currentUser.displayName;
                                else {
                                    const combineduId1 = currentUser.uid > c.commenter ? currentUser.uid+c.commenter : c.commenter+currentUser.uid;
                                    var chatinfo = (userchats.filter(u => {return u[0] === combineduId1}))[0];
                                    if (chatinfo !== undefined) {
                                        if (chatinfo[1].userInfo.alias === "")
                                            commenteralias = chatinfo[1].userInfo.displayName;
                                        else {
                                            commenteralias = chatinfo[1].userInfo.alias;
                                        }
                                    } else {
                                        commenteralias = null;
                                    }
                                }
                                var becommenteralias = "";
                                if (c.becommenter !== "") {
                                    if (c.becommenter === currentUser.uid)
                                        becommenteralias = currentUser.displayName;
                                    else {
                                        const combineduId2 = currentUser.uid > c.becommenter ? currentUser.uid+c.becommenter : c.becommenter+currentUser.uid;
                                        chatinfo = (userchats.filter(u => {return u[0] === combineduId2}))[0];
                                        if (chatinfo !== undefined) {
                                            if (chatinfo[1].userInfo.alias === "")
                                                becommenteralias = chatinfo[1].userInfo.displayName;
                                            else {
                                                becommenteralias = chatinfo[1].userInfo.alias;
                                            }
                                        } else {
                                            becommenteralias = null;
                                        }
                                    }
                                }
                                if (commenteralias !== null && becommenteralias !== null) {
                                    return (
                                        <div className='comment' key={`moment${index}comment${i}`}>
                                            <span className={`useralias ${c.commenter === currentUser.uid && "self"}`} onClick={() => replycomment(c.commenter, commenteralias)}>{commenteralias}</span>
                                            {(becommenteralias !== "") && <span> @ </span>}
                                            <span className={`useralias ${c.becommenter === currentUser.uid && "self"}`} onClick={() => replycomment(c.becommenter, becommenteralias)}>{becommenteralias}</span>
                                            <span>: {c.content}</span>
                                        </div>
                                    )
                                }
                                return null;
                            })}
                        </div>
                        {(displayAlias.length !== 0 || checkcomments) && commentmode && <div className='seperateline'></div>}
                        {commentmode && <div className='commentinput'>
                            <textarea placeholder={replyuser} value={commenttext} onChange={e => setCommenttext(e.target.value)} />
                            <div className='emojiandbutton'>
                                <i className="fa-regular fa-face-smile" id='emojiicon' onClick={() => setEmojiSelect(true)}></i>
                                {emojiSelect && <div id="emojipicker">
                                    <Picker data={data} onEmojiSelect={(emoji) => {
                                        setCommenttext(commenttext + emoji.native);
                                        setEmojiSelect(false);
                                    }} />
                                </div>}
                                <button onClick={handleSend}>Send</button>
                            </div>
                        </div>}
                    </div>}
                </div>
            </div>
        )
}

export default Moment;
