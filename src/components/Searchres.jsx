import { doc, serverTimestamp, updateDoc } from "firebase/firestore";
import React, { useContext } from "react";
import { useState } from "react";
import { AuthContext } from "../context/AuthContext";
import { ChatContext } from "../context/ChatContext";
import { db } from "../firebase";


const Searchres = ({res, index, friendlist, setErr, setSearchlength, setUser, setUsernameore}) => {

    const {currentUser} = useContext(AuthContext);
    const {dispatch} = useContext(ChatContext);

    const [addnotemode, setAddnotemode] = useState(false);
    const [notes, setNotes] = useState("Hi! I am "+currentUser.displayName+".");

    var searchres = res.data();

    const checknewfriend = (searchres) => {
        var check = friendlist.filter(f => {return f === searchres.uid});
        if (check.length === 0)
            return true;
        else
            return false;
    };

    const handleSelect = async (user) => {

        dispatch({
            type: "CHANGE_USER",
            payload: user
        });

        const combineduId = currentUser.uid > user.uid ? currentUser.uid+user.uid : user.uid+currentUser.uid;

        try {
            await updateDoc(doc(db, "userChats", currentUser.uid), {
                [combineduId+".date"]: serverTimestamp(),
            });
        } catch(err) {
            setErr(true);
        }

        setSearchlength(0);
        setUser(null);
        setUsernameore("");
    };

    const newfriendApplication = async (user) => {
        try {
            await updateDoc(doc(db, "newfriendsApplication", user.uid), {
                [currentUser.uid+".userInfo"]: {
                    uid: currentUser.uid,
                    displayName: currentUser.displayName,
                    photoURL: currentUser.photoURL,
                    email: currentUser.email,
                },
                [currentUser.uid+".notes"]: notes,
                [currentUser.uid+".date"]: serverTimestamp(),
            });
        } catch(err) {
            setErr(true);
        }

        setSearchlength(0);
        setUser(null);
        setUsernameore("");
        setNotes("");
        setAddnotemode(false);
    };

    window.addEventListener("click", e => {
        const addnote = document.getElementById("addnotes"+searchres.uid);
        const noteinfo = document.getElementById("notepad"+searchres.uid);
        if (!noteinfo?.contains(e.target) && e.target !== addnote) {
            setAddnotemode(false);
        }
    });


    return (
        <div className={`userChat ${!checknewfriend(searchres) && "oldfriend"}`} onClick={() => {
            if (!checknewfriend(searchres))
                handleSelect(searchres);
        }}>
            <img src={searchres.photoURL} alt="" />
            <div className="userChatInfo">
                <span>{searchres.displayName}</span>
                <span className="useremail">{searchres.email}</span>
            </div>
            {checknewfriend(searchres) && <i className="addfriend fa-solid fa-user-plus" onClick={() => newfriendApplication(searchres)} ></i>}
            {checknewfriend(searchres) && <i className="addnotes fa-regular fa-note-sticky" id={`addnotes${searchres.uid}`} onClick={() => setAddnotemode(!addnotemode)}></i>}
            <div className={`noteinfo ${addnotemode && "show"}`} id={`notepad${searchres.uid}`}>
                <textarea placeholder={"Add notes: Hi! I am "+currentUser.displayName+"..."} onChange={e=>setNotes(e.target.value)} value={notes} />
            </div>
        </div>
    )
};

export default Searchres;
