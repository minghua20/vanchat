import { deleteField, doc, getDoc, serverTimestamp, setDoc, updateDoc } from "firebase/firestore";
import React, { useContext } from "react";
import { useState } from "react";
import { AuthContext } from "../context/AuthContext";
import { ChatContext } from "../context/ChatContext";
import { db } from "../firebase";


const Newfriendinfo = ({chat, newfriendslength, setNewfriendslength}) => {

    const {dispatch} = useContext(ChatContext);
    const {currentUser} = useContext(AuthContext);

    const [shownotes, setShownotes] = useState(false);


    const handleSelect = async (user) => {
        dispatch({
            type: "CHANGE_USER",
            payload: user
        });

        const combineduId = currentUser.uid > user.uid ? currentUser.uid+user.uid : user.uid+currentUser.uid;

        try {
            await setDoc(doc(db, "chats", combineduId), {messages: []});

            await updateDoc(doc(db, "userChats", currentUser.uid), {
                [combineduId+".userInfo"]: {
                    uid: user.uid,
                    displayName: user.displayName,
                    photoURL: user.photoURL,
                    email: user.email,
                    alias: "",
                },
                [combineduId+".date"]: serverTimestamp(),
                [combineduId+".unread"]: 0,
            });

            await updateDoc(doc(db, "userChats", user.uid), {
                [combineduId+".userInfo"]: {
                    uid: currentUser.uid,
                    displayName: currentUser.displayName,
                    photoURL: currentUser.photoURL,
                    email: currentUser.email,
                    alias: "",
                },
                [combineduId+".date"]: serverTimestamp(),
                [combineduId+".unread"]: 0,
            });

            var res = await getDoc(doc(db, "users", currentUser.uid));
            var useri = res.data();
            var friendlist = useri.friendlist;
            friendlist = friendlist.concat(user.uid);
            await updateDoc(doc(db, "users", currentUser.uid), {
                friendlist: friendlist,
            });

            if (currentUser.uid !== user.uid) {
                res = await getDoc(doc(db, "users", user.uid));
                useri = res.data();
                friendlist = useri.friendlist;
                friendlist = friendlist.concat(currentUser.uid);
                await updateDoc(doc(db, "users", user.uid), {
                    friendlist: friendlist,
                });
            }

            await updateDoc(doc(db, "newfriendsApplication", currentUser.uid), {
                [user.uid]: deleteField(),
            });

        } catch(err) {
            console.log(err);
        }

        if ((newfriendslength - 1) > 2)
            setNewfriendslength(2);
        else
            setNewfriendslength(newfriendslength - 1);
    };

    const handleRefuse = async (user) => {
        await updateDoc(doc(db, "newfriendsApplication", currentUser.uid), {
            [user.uid]: deleteField(),
        });
        if ((newfriendslength - 1) > 2)
            setNewfriendslength(2);
        else
            setNewfriendslength(newfriendslength - 1);
    };

    window.addEventListener("click", e => {
        const noteicon = document.getElementById("noteicon"+chat[1].userInfo.uid);
        const noteinfo = document.getElementById("noteinfo"+chat[1].userInfo.uid);

        if (!noteinfo?.contains(e.target) && e.target !== noteicon) {
            setShownotes(false);
        }
    });


    return (
        <div className="userChat">
            <div className="userphoto">
                <img src={chat[1].userInfo.photoURL} alt="" />
            </div>
            <div className="userChatInfo">
                <span className="username">{chat[1].userInfo.displayName}</span>
                <span className="useremail">{chat[1].userInfo.email}</span>
            </div>
            <i className="noteicon fa-regular fa-note-sticky" id={`noteicon${chat[1].userInfo.uid}`} onClick={() => setShownotes(!shownotes)}></i>
            <i className="accepticon fa-regular fa-circle-check" onClick={() => {handleSelect(chat[1].userInfo)}}></i>
            <i className="refuseicon fa-regular fa-circle-xmark" onClick={() => handleRefuse(chat[1].userInfo)}></i>
            <div className={`noteinfo ${shownotes && "show"}`} id={`noteinfo${chat[1].userInfo.uid}`}>
                <div className="notebox">
                    <span className="defaultwords">Newfriend's notes: </span>
                    <span>{chat[1].notes}</span>
                </div>
            </div>
        </div>
    )
};

export default Newfriendinfo;
