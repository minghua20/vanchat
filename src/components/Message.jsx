import React, { useContext } from "react";
import { useEffect } from "react";
import { useRef } from "react";
import { AuthContext } from "../context/AuthContext";
import { ChatContext } from "../context/ChatContext";


const getDisplayTime = (messagetime) => {
    var displayTime;
    var currentTime = new Date();
    if (messagetime.Year !== currentTime.getFullYear()) {
        displayTime = messagetime.Year + '-' + messagetime.Month + '-' + messagetime.Date + ' ' + messagetime.Hour + ":" + messagetime.Minute;
    }
    else {
        if (messagetime.Month !== currentTime.getMonth()+1) {
            displayTime = messagetime.Month + '-' + messagetime.Date + ' ' + messagetime.Hour + ":" + messagetime.Minute;
        }
        else {
            if (messagetime.Date !== currentTime.getDate()) {
                displayTime = messagetime.Month + '-' + messagetime.Date + ' ' + messagetime.Hour + ":" + messagetime.Minute;
            }
            else {
                displayTime = messagetime.Hour + ":" + messagetime.Minute;
            }
        }
    }

    return displayTime;
}


const Message = ({message, index, premessage, length, imgcounter, setImgcounter}) => {

    const {currentUser} = useContext(AuthContext);
    const {chatdata} = useContext(ChatContext);

    const ref = useRef();

    useEffect(() => {
        if (index === (length - 1))
            ref.current?.scrollIntoView({behavior: "smooth"});    
    }, [message, index, length]);

    var preTime;
    if (premessage !== undefined)
        preTime = getDisplayTime(premessage.displayTime);
    else
        preTime = "null";
    var displayTime = getDisplayTime(message.displayTime);


    return (
        <>
        {(preTime !== displayTime) && <div className="messagetime">
            <span>{displayTime}</span>
        </div>}
        <div className={`message ${message.senderId === currentUser.uid && "owner"}`}>
            <div className="messageInfo">
                <img src={message.senderId === currentUser.uid ? currentUser.photoURL : chatdata.user.photoURL} alt="" />
            </div>
            <div ref={ref} className="messageContent">
                {(message.text !== "") && <p>{message.text}</p>}
                {message.type === "image" && <img className="messageImg" onLoad={() => {
                    setImgcounter(imgcounter + 1);
                    if (index === (length - 1)) 
                        ref.current?.scrollIntoView({behavior: "smooth"});
                }} src={message.file} alt="" onClick={()=>{window.open(message.file, '_blank', 'width=770,height=515,top=145,left=380')}} />}
                {message.type === "video" && <video className="videos" controls src={message.file} />}
                {message.type === "audio" && <div className="audiofile">
                    <span>{message.audioname}</span>
                    <audio className="audios" controls src={message.file} />
                </div>}
                {message.type === "voicemessage" && <div className="audiowrapper">
                    <audio className="audios" controls src={message.file} />
                </div>}
            </div>
        </div>
        </>
    )
}

export default Message;
