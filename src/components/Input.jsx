import React, { useContext } from "react";
import { useState } from "react";
import { AuthContext } from "../context/AuthContext";
import { ChatContext } from "../context/ChatContext";
// import Attach from "../imgs/attach.png"
// import Img from "../imgs/img.png"
import { v4 as uuid } from "uuid";
import { arrayUnion, doc, getDoc, serverTimestamp, Timestamp, updateDoc } from "firebase/firestore";
import { db, storage } from "../firebase";
import { getDownloadURL, ref, uploadBytesResumable } from "firebase/storage";
import data from '@emoji-mart/data';
import Picker from '@emoji-mart/react';
import AudioWaveRecorder from "./AudioWaveRecorder";


const getMessageTime = () => {
    var time = new Date();
    var Hours = time.getHours();
    if (Hours < 10)
        Hours = '0' + Hours;
    var Minutes = time.getMinutes();
    if (Minutes < 10)
        Minutes = '0' + Minutes;
    var Seconds = time.getSeconds();
    if (Seconds < 10)
        Seconds = '0' + Seconds;
    
    var datestring = {
        Year: time.getFullYear(),
        Month: time.getMonth() + 1,
        Date: time.getDate(),
        Hour: Hours,
        Minute: Minutes,
        Second: Seconds,
    };

    return datestring;
}

const Input = () => {

    const [text, setText] = useState("");
    const [files, setFiles] = useState([]);
    const [emojiSelect, setEmojiSelect] = useState(false);
    const [uploadprogress, setUploadprogress] = useState(0);
    const [audiomode, setAudiomode] = useState(false);
    const [audiosrc, setAudiosrc] = useState(null);
    const [audioBlob, setAudioBlob] = useState(null);

    const {currentUser} = useContext(AuthContext);
    const {chatdata} = useContext(ChatContext);


    const handleSend = async () => {

        if (audiomode && audioBlob) {
            const storageRef = ref(storage, uuid());
            const uploadTask = uploadBytesResumable(storageRef, audioBlob);

            uploadTask.on(
                "state_changed",
                (snapshot) => {
                    const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                    switch (snapshot.state) {
                        case 'paused':
                        console.log('Upload is paused');
                        break;
                        case 'running':
                        // console.log('Upload is running');
                        break;
                        default:
                        break;
                    }
                    console.log("Upload is " + progress + "% done");
                    setUploadprogress(progress);
                },
                (error) => {
                    // setErrMessage(error.message)
                    console.log(error.message);
                },
                // eslint-disable-next-line no-loop-func
                () => {
                    getDownloadURL(uploadTask.snapshot.ref).then(async (downloadURL) => {
                        var res = await getDoc(doc(db, "userChats", chatdata.user.uid));
                        var preuserchats = Object.entries(res.data());
                        var prelastm = preuserchats.filter(u => u[0] === chatdata.chatId);
                        var messageunread = prelastm[0][1].unread;

                        await updateDoc(doc(db, "chats", chatdata.chatId), {
                            messages: arrayUnion({
                                id: uuid(),
                                text: "",
                                senderId: currentUser.uid,
                                date: Timestamp.now(),
                                displayTime: getMessageTime(),
                                file: downloadURL,
                                type: "voicemessage",
                            }),
                        });

                        await updateDoc(doc(db, "userChats", currentUser.uid), {
                            [chatdata.chatId + ".lastMessage"]: {
                                text: "[Voice message]",
                            },
                            [chatdata.chatId + ".date"]: serverTimestamp(),
                            [chatdata.chatId + ".displayTime"]: getMessageTime(),
                        });
                
                        await updateDoc(doc(db, "userChats", chatdata.user.uid), {
                            [chatdata.chatId + ".lastMessage"]: {
                                text: "[Voice message]",
                            },
                            [chatdata.chatId + ".date"]: serverTimestamp(),
                            [chatdata.chatId + ".displayTime"]: getMessageTime(),
                            [chatdata.chatId + ".unread"]: messageunread + 1,
                        });

                        setAudiosrc(null);
                        setAudioBlob(null);
                        return;
                    });
                }
            )
        }

        let checkBlankText = true;
        for (let i = 0; i < text.length; i++) {
            if (text[i] !== " ") {
                checkBlankText = false;
                break;
            }
        }

        if (checkBlankText && (text !== ""))
            alert("Message to be sent cannot be empty!");

        if (files.length !== 0) {
            if (!checkBlankText) {
                await updateDoc(doc(db, "chats", chatdata.chatId), {
                    messages: arrayUnion({
                        id: uuid(),
                        text,
                        senderId: currentUser.uid,
                        date: Timestamp.now(),
                        displayTime: getMessageTime(),
                    }),
                });
            }

            var uploadcounter = 0;
            for (let i = 0; i < files.length; i++) {
                const storageRef = ref(storage, uuid());
                const uploadTask = uploadBytesResumable(storageRef, files[i]);

                uploadTask.on(
                    "state_changed",
                    (snapshot) => {
                        const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                        switch (snapshot.state) {
                            case 'paused':
                            console.log('Upload is paused');
                            break;
                            case 'running':
                            // console.log('Upload is running');
                            break;
                            default:
                            break;
                        }
                        console.log("Upload is " + progress + "% done");
                        setUploadprogress(progress);
                    },
                    (error) => {
                        // setErrMessage(error.message)
                        console.log(error.message);
                    },
                    // eslint-disable-next-line no-loop-func
                    () => {
                        getDownloadURL(uploadTask.snapshot.ref).then(async (downloadURL) => {
                            var res = await getDoc(doc(db, "userChats", chatdata.user.uid));
                            var preuserchats = Object.entries(res.data());
                            var prelastm = preuserchats.filter(u => u[0] === chatdata.chatId);
                            var messageunread = prelastm[0][1].unread;

                            if (files[i].type[0] === "i") {
                                await updateDoc(doc(db, "chats", chatdata.chatId), {
                                    messages: arrayUnion({
                                        id: uuid(),
                                        text: "",
                                        senderId: currentUser.uid,
                                        date: Timestamp.now(),
                                        displayTime: getMessageTime(),
                                        file: downloadURL,
                                        type: "image",
                                    }),
                                });

                                await updateDoc(doc(db, "userChats", currentUser.uid), {
                                    [chatdata.chatId + ".lastMessage"]: {
                                        text: "[img]",
                                    },
                                    [chatdata.chatId + ".date"]: serverTimestamp(),
                                    [chatdata.chatId + ".displayTime"]: getMessageTime(),
                                });
                        
                                await updateDoc(doc(db, "userChats", chatdata.user.uid), {
                                    [chatdata.chatId + ".lastMessage"]: {
                                        text: "[img]",
                                    },
                                    [chatdata.chatId + ".date"]: serverTimestamp(),
                                    [chatdata.chatId + ".displayTime"]: getMessageTime(),
                                    [chatdata.chatId + ".unread"]: messageunread + 1,
                                });

                                uploadcounter++;
                                if (uploadcounter === files.length) {
                                    const imginput = document.getElementById("imginput");
                                    imginput.value = "";
                                }
                            }
                            else if (files[i].type[0] === "v") {
                                await updateDoc(doc(db, "chats", chatdata.chatId), {
                                    messages: arrayUnion({
                                        id: uuid(),
                                        text: "",
                                        senderId: currentUser.uid,
                                        date: Timestamp.now(),
                                        displayTime: getMessageTime(),
                                        file: downloadURL,
                                        type: "video",
                                    }),
                                });

                                await updateDoc(doc(db, "userChats", currentUser.uid), {
                                    [chatdata.chatId + ".lastMessage"]: {
                                        text: "[video]",
                                    },
                                    [chatdata.chatId + ".date"]: serverTimestamp(),
                                    [chatdata.chatId + ".displayTime"]: getMessageTime(),
                                });
                        
                                await updateDoc(doc(db, "userChats", chatdata.user.uid), {
                                    [chatdata.chatId + ".lastMessage"]: {
                                        text: "[video]",
                                    },
                                    [chatdata.chatId + ".date"]: serverTimestamp(),
                                    [chatdata.chatId + ".displayTime"]: getMessageTime(),
                                    [chatdata.chatId + ".unread"]: messageunread + 1,
                                });

                                uploadcounter++;
                                if (uploadcounter === files.length) {
                                    const imginput = document.getElementById("imginput");
                                    imginput.value = "";
                                }
                            }
                            else if (files[i].type[0] === "a") {
                                await updateDoc(doc(db, "chats", chatdata.chatId), {
                                    messages: arrayUnion({
                                        id: uuid(),
                                        text: "",
                                        senderId: currentUser.uid,
                                        date: Timestamp.now(),
                                        displayTime: getMessageTime(),
                                        file: downloadURL,
                                        type: "audio",
                                        audioname: files[i].name,
                                    }),
                                });

                                await updateDoc(doc(db, "userChats", currentUser.uid), {
                                    [chatdata.chatId + ".lastMessage"]: {
                                        text: "[audio]",
                                    },
                                    [chatdata.chatId + ".date"]: serverTimestamp(),
                                    [chatdata.chatId + ".displayTime"]: getMessageTime(),
                                });
                        
                                await updateDoc(doc(db, "userChats", chatdata.user.uid), {
                                    [chatdata.chatId + ".lastMessage"]: {
                                        text: "[audio]",
                                    },
                                    [chatdata.chatId + ".date"]: serverTimestamp(),
                                    [chatdata.chatId + ".displayTime"]: getMessageTime(),
                                    [chatdata.chatId + ".unread"]: messageunread + 1,
                                });

                                uploadcounter++;
                                if (uploadcounter === files.length) {
                                    const imginput = document.getElementById("imginput");
                                    imginput.value = "";
                                }
                            }
                        });
                    }
                );
            }
        }
        else {
            if (text !== "" && (!checkBlankText)) {
                await updateDoc(doc(db, "chats", chatdata.chatId), {
                    messages: arrayUnion({
                        id: uuid(),
                        text,
                        senderId: currentUser.uid,
                        date: Timestamp.now(),
                        displayTime: getMessageTime(),
                    }),
                });

                var res = await getDoc(doc(db, "userChats", chatdata.user.uid));
                var preuserchats = Object.entries(res.data());
                var prelastm = preuserchats.filter(u => u[0] === chatdata.chatId);
                var messageunread = prelastm[0][1].unread;

                await updateDoc(doc(db, "userChats", currentUser.uid), {
                    [chatdata.chatId + ".lastMessage"]: {
                        text,
                    },
                    [chatdata.chatId + ".date"]: serverTimestamp(),
                    [chatdata.chatId + ".displayTime"]: getMessageTime(),
                });
        
                await updateDoc(doc(db, "userChats", chatdata.user.uid), {
                    [chatdata.chatId + ".lastMessage"]: {
                        text,
                    },
                    [chatdata.chatId + ".date"]: serverTimestamp(),
                    [chatdata.chatId + ".displayTime"]: getMessageTime(),
                    [chatdata.chatId + ".unread"]: messageunread + 1,
                });
            }
        }

        setText("");
        setFiles([]);
    };


    const handleKey = (e) => {
        e.code === "Enter" && handleSend();
    }

    const updateunread = async () => {
        if (chatdata.user.uid !== undefined) {
            await updateDoc(doc(db, "userChats", currentUser.uid), {
                [chatdata.chatId + ".unread"]: 0,
            });
        }
    }

    window.addEventListener('paste', e => {
        // const imgInput = document.getElementById("imginput");
        // imgInput.files = e.clipboardData.files;
        setFiles(e.clipboardData.files);
    });

    window.addEventListener("click", e => {
        const emojipicker = document.getElementById("emojipicker");
        const emojiicon = document.getElementById("emojiicon");
        if (!emojipicker?.contains(e.target) && e.target !== emojiicon)
            setEmojiSelect(false);
    });


    return (
        <div className="input" onClick={() => updateunread()}>
            <div className="filesandemoji">
                <div className="icons">
                    {/* <img src={Attach} alt="" /> */}
                    <input type="file" accept="image/*, video/*, audio/*" id="imginput" multiple onChange={e=>setFiles(e.target.files)} />
                    <label htmlFor="imginput">
                        {files.length === 0 && <i className="fa-regular fa-folder-closed"></i>}
                        {files.length > 0 && <i className="fa-solid fa-file-arrow-up"></i>}
                        {files.length > 0 && files.length < 10 && <div className="freddot">
                            <span className="fileslength">{files.length}</span>
                        </div>}
                        {files.length >= 10 && <div className="freddot">
                            <span className="fileslength long">..</span>
                        </div>}
                        {/* <img src={Img} alt="" />
                        {(files.length !== 0) && <div className="imgreddot"></div>} */}
                    </label>
                    <i className="fa-regular fa-face-smile" id="emojiicon" onClick={() => setEmojiSelect(true)}></i>
                    {emojiSelect && <div id="emojipicker">
                        <Picker data={data} onEmojiSelect={(emoji) => {
                            setText(text + emoji.native);
                        }} />
                    </div>}
                    <div className={`uploadprogress ${(uploadprogress !== 0 && uploadprogress !== 100 && "show")}`}>
                        <i className="fa fa-spinner fa-spin"></i>
                        <span>Upload is {uploadprogress}% done</span>
                    </div>
                </div>
                <div className="micandkeyboad" onClick={() => {
                    if (!audiomode) {
                        if (navigator.mediaDevices.getUserMedia) {
                            setAudiomode(true);
                        }
                        else {
                            alert('Voice message is not supported on your browser!');
                        }
                    }
                    else
                        setAudiomode(false);
                }}>
                    {!audiomode && <i className="fa-solid fa-microphone"></i>}
                    {audiomode && <i className="keyboard fa-regular fa-keyboard"></i>}
                </div>
            </div>
            <div className="contentbutton">
                {!audiomode && <textarea placeholder="Type something here..." value={text} onKeyDown={handleKey} onChange={e=>setText(e.target.value)} />}
                {audiomode && <div></div>}
                {audiomode && <AudioWaveRecorder audiosrc={audiosrc} setAudiosrc={setAudiosrc} setAudioBlob={setAudioBlob} />}
                <button className="sendbutton" onClick={handleSend}>Send</button>
            </div>
        </div>
    )
}

export default Input;
