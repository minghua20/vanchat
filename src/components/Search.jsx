import React, { useState } from "react";
import { collection, doc, getDoc, getDocs, query, where } from "firebase/firestore";
import { db } from "../firebase";
import { useContext } from "react";
import { AuthContext } from "../context/AuthContext";
import { useNavigate } from "react-router-dom";
import Searchres from "./Searchres";
import { AiOutlineUserAdd } from "react-icons/ai";


const Search = ({setSearchlength}) => {

    const navigate = useNavigate();

    const [usernameore, setUsernameore] = useState("");
    const [user, setUser] = useState(null);
    const [err, setErr] = useState(false);

    const {currentUser} = useContext(AuthContext);
    const [friendlist, setFriendlist] = useState([]);


    const handleSearch = async () => {
        const q1 = query(
            collection(db, "users"),
            where("displayName", "==", usernameore)
        );

        const q2 = query(
            collection(db, "users"),
            where("email", "==", usernameore)
        );

        try {
            const querySnapshot = await getDocs(q1);
            if (querySnapshot.docs.length !== 0) {
                setUser(querySnapshot.docs);
                if (querySnapshot.docs.length > 2)
                    setSearchlength(2);
                else
                    setSearchlength(querySnapshot.docs.length);
            }
            else {
                const querySnapshote = await getDocs(q2);
                if (querySnapshote.docs.length !== 0) {
                    setUser(querySnapshote.docs);
                    if (querySnapshot.docs.length > 2)
                        setSearchlength(2);
                    else
                        setSearchlength(querySnapshote.docs.length);
                }
                else {
                    setUser(null);
                    setSearchlength(0);
                    return;
                }
            }

            var res = await getDoc(doc(db, "users", currentUser.uid));
            var useri = res.data();
            setFriendlist(useri.friendlist);

        } catch(err) {
            setErr(true);
        }
    };

    const handleKey = (e) => {
        e.code === "Enter" && handleSearch();
    }

    const clearinput = () => {
        setSearchlength(0);
        setUser(null);
        setUsernameore("");
    }


    return (
        <div className="search">
            <div className="searchForm">
                <AiOutlineUserAdd className="usericon" />
                <input type="text" placeholder="Search username / email:" onKeyDown={handleKey} onChange={e=>setUsernameore(e.target.value)} value={usernameore} />
                {((usernameore !== "") || (user !== null)) && <i className="closeicon fa-solid fa-xmark" onClick={() => {clearinput()}}></i>}
                {((usernameore !== "") || (user !== null)) && <span className="fixbuffer"></span>}
                <i className="momentsicon fa-solid fa-icons" onClick={() => navigate("/moments")}></i>
            </div>
            {err && <span className="errorMessage">Oh! An error occurs!</span>}
            <div className="searchres">
                {user?.map((res, index) => (
                    <Searchres res={res} index={index} friendlist={friendlist} setErr={setErr} setSearchlength={setSearchlength} setUser={setUser} setUsernameore={setUsernameore} key={res.data().uid} />
                ))}
            </div>
        </div>
    )
}

export default Search;
