import React from "react";
import { useNavigate, Link } from "react-router-dom";
import { useState } from "react";
import { signInWithEmailAndPassword } from "firebase/auth";
import { auth } from "../firebase";


const Login = () => {

    const [errMessage, setErrMessage] = useState('');
    const [isLoading, setIsLoading] = useState(false);
    const navigate = useNavigate();

    const getlink = () => {
        if (isLoading)
            return "";
        else
            return "/register";
    };

    const handleSubmit = async(e) => {
        setIsLoading(true);

        e.preventDefault()
        const email = e.target[0].value;
        const password = e.target[1].value;

        try {
            await signInWithEmailAndPassword(auth, email, password)
            navigate("/");
            setIsLoading(false);
        } catch(err) {
            setIsLoading(false);

            if (err.message === "Firebase: Error (auth/wrong-password).")
                setErrMessage("Password is wrong.");
            else if (err.message === "Firebase: Access to this account has been temporarily disabled due to many failed login attempts. You can immediately restore it by resetting your password or you can try again later. (auth/too-many-requests).")
                setErrMessage("Access to this account has been temporarily disabled due to many failed login attempts. Please try again later.");
            else if (err.message === "Firebase: Error (auth/user-not-found).")
                setErrMessage("Account not found.")
            else if (err.message === "Firebase: Error (auth/invalid-email).")
                setErrMessage("Invalid email.");
            else
                setErrMessage(err.code);
        }
    };


    return (
        <div className="formContainer">
            <div className="blob"></div>
            <div className="formWrapper">
                <span className="logo">VanChat</span>
                <span className="title">Login</span>
                <form onSubmit={handleSubmit}>
                    <input type="email" placeholder="Email" />
                    <input type="password" placeholder="Password" />
                    {errMessage && (<span className="errorMessage">{errMessage}</span>)}
                    <button disabled={isLoading}>Sign in{isLoading && <i className="fa fa-circle-o-notch fa-spin"></i>}</button>
                </form>
                <p>New VanChatter? <Link to={getlink()}>Register now!</Link></p>
            </div>
        </div>
    )
}

export default Login;
