import { doc, serverTimestamp, updateDoc } from 'firebase/firestore';
import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom';
import { AuthContext } from '../context/AuthContext';
import { storage, db } from '../firebase';
// import Picture from "../imgs/addAvatar.png"
import { useContext } from "react";
import { ref, uploadBytesResumable, getDownloadURL } from "firebase/storage";
import { v4 as uuid } from "uuid";
import { useEffect } from 'react';
import data from '@emoji-mart/data';
import Picker from '@emoji-mart/react';


const getCurrentTime = () => {
    var time = new Date();
    var Hours = time.getHours();
    if (Hours < 10)
        Hours = '0' + Hours;
    var Minutes = time.getMinutes();
    if (Minutes < 10)
        Minutes = '0' + Minutes;
    var Seconds = time.getSeconds();
    if (Seconds < 10)
        Seconds = '0' + Seconds;
    
    var datestring = {
        Year: time.getFullYear(),
        Month: time.getMonth() + 1,
        Date: time.getDate(),
        Hour: Hours,
        Minute: Minutes,
        Second: Seconds,
    };

    return datestring;
}


const CreateMoment = () => {

    const navigate = useNavigate();
    const {currentUser} = useContext(AuthContext);
    const [textinput, setTextinput] = useState("");
    const [file, setFile] = useState(null);
    const [imglist, setImglist] = useState([]);
    const [video, setVideo] = useState(null);
    const [audio, setAudio] = useState(null);
    const [audioname, setAudioname] = useState(null);
    const [videolock, setVideolock] = useState(false);
    const [addmode, setAddmode] = useState(true);
    const [uploadprogress, setUploadprogress] = useState(0);
    const [emojiSelect, setEmojiSelect] = useState(false);


    window.addEventListener("click", e => {
        const emojipicker = document.getElementById("emojis");
        const emojiicon = document.getElementById("emojiicon");
        if (!emojipicker?.contains(e.target) && e.target !== emojiicon)
            setEmojiSelect(false);
    });

    const imagePopup = (img) => {
        window.open(img, '_blank', 'width=770,height=515,top=145,left=380');
    };

    const handlePost = async () => {
        try {
            // if (textinput === "" && imglist.length === 0 && video === null) {
            //     navigate("/moments");
            //     return;
            // }

            var momentid = uuid();
            var datestring = getCurrentTime();
            if (audio) {
                await updateDoc(doc(db, "moments", currentUser.uid), {
                    [momentid+".momentInfo"]: {
                        uid: currentUser.uid,
                        displayName: currentUser.displayName,
                        photoURL: currentUser.photoURL,
                        text: textinput,
                        audio: audio,
                        audioname: audioname,
                    },
                    [momentid+".likes"]: [],
                    [momentid+".comments"]: [],
                    [momentid+".date"]: serverTimestamp(),
                    [momentid+".displaydate"]: datestring,
                });
            }
            else if (video) {
                await updateDoc(doc(db, "moments", currentUser.uid), {
                    [momentid+".momentInfo"]: {
                        uid: currentUser.uid,
                        displayName: currentUser.displayName,
                        photoURL: currentUser.photoURL,
                        text: textinput,
                        video: video,
                    },
                    [momentid+".likes"]: [],
                    [momentid+".comments"]: [],
                    [momentid+".date"]: serverTimestamp(),
                    [momentid+".displaydate"]: datestring,
                });
            }
            else {
                await updateDoc(doc(db, "moments", currentUser.uid), {
                    [momentid+".momentInfo"]: {
                        uid: currentUser.uid,
                        displayName: currentUser.displayName,
                        photoURL: currentUser.photoURL,
                        text: textinput,
                        picture: imglist,
                    },
                    [momentid+".likes"]: [],
                    [momentid+".comments"]: [],
                    [momentid+".date"]: serverTimestamp(),
                    [momentid+".displaydate"]: datestring,
                });
            }
            setTextinput("");
            setFile(null);
            setVideo(null);
            setAudio(null);
            setAudioname(null);
            navigate("/moments");
        } catch(err) {
            console.log(err.message);
        }
    }

    const addimg = () => {
        if (file !== null && file !== undefined) {

            var uploadlength;
            if (imglist.length + file.length >= 9) {
                if (imglist.length + file.length > 9)
                    alert("Upload a maximum of 9 images!")
                uploadlength = 9 - imglist.length;
                setAddmode(false);
            }
            else {
                uploadlength = file.length;
            }

            var uploadcounter = 0;
            for (let i = 0; i < uploadlength; i++) {

                const storageRef = ref(storage, uuid());
                const uploadTask = uploadBytesResumable(storageRef, file[i]);

                uploadTask.on(
                    "state_changed",
                    (snapshot) => {
                        const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                        switch (snapshot.state) {
                            case 'paused':
                            console.log('Upload is paused');
                            break;
                            case 'running':
                            // console.log('Upload is running');
                            break;
                            default:
                            break;
                        }
                        console.log("Upload is " + progress + "% done");
                        setUploadprogress(progress);
                    },
                    (error) => {
                        console.log(error.message);
                    }, 
                    // eslint-disable-next-line no-loop-func
                    () => {
                        getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
                            if (imglist.length === 0) {
                                if (file[0].type[0] === "i")
                                    setVideolock(true);
                                else if (file[0].type[0] === "v") {
                                    setVideo(downloadURL);
                                    setAddmode(false);
                                    setFile(null);
                                    const uploadbutton = document.getElementById("uploadbutton");
                                    uploadbutton.value = "";
                                    return;
                                }
                                else if (file[0].type[0] === "a") {
                                    setAudio(downloadURL);
                                    setAudioname(file[0].name);
                                    setAddmode(false);
                                    setFile(null);
                                    const uploadbutton = document.getElementById("uploadbutton");
                                    uploadbutton.value = "";
                                    return;
                                }
                            }
                            uploadcounter++;
                            var ilist = imglist;
                            ilist.push(downloadURL);
                            setImglist(ilist);
                            if (uploadcounter === uploadlength) {
                                setFile(null);
                                const uploadbutton = document.getElementById("uploadbutton");
                                uploadbutton.value = "";
                            }
                        });
                    }
                );
            }
        }
    };

    const deleteimg = (img) => {
        var newimglist = imglist.filter(i => {return i !== img});
        if (newimglist.length < 9)
            setAddmode(true);
        if (newimglist.length === 0)
            setVideolock(false);
        setImglist(newimglist);
    };
    
    useEffect(() => {
        addimg();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [file]);


    return (
        <div className="Moments">
            <div className="Momentcontainer post">
                {emojiSelect && <div id="emojis">
                    <Picker data={data} onEmojiSelect={(emoji) => setTextinput(textinput + emoji.native)} />
                </div>}
                <div className="Momentheader">
                    <i onClick={()=>navigate("/moments")} className="fa-solid fa-chevron-left"></i>
                    <p>Moments</p>
                    <button disabled={(textinput === "" && imglist.length === 0 && video === null && audio === null)} onClick={handlePost}>Post</button>
                </div>
                <textarea placeholder="Thought of this moment..." onChange={e=>setTextinput(e.target.value)} value={textinput} />
                <div className='emojiandupload'>
                    <i className="fa-regular fa-face-smile" id='emojiicon' onClick={() => setEmojiSelect(true)}></i>
                    <span className={`uploadprogress ${(uploadprogress !== 0 && uploadprogress !== 100 && "show")}`}>
                        <i className="fa fa-spinner fa-spin"></i>
                        <span>Upload is {uploadprogress}% done</span>
                    </span>
                </div>
                <div className='addingarea'>
                    {video && <div className='previewvideo'>
                        <video controls src={video} />
                        <i className="fa-solid fa-trash-can" onClick={() => {
                            setVideo(null);
                            setAddmode(true);
                        }}></i>
                    </div>}
                    {audio && <div className='previewaudio'>
                        <audio controls src={audio} />
                        <i className="fa-solid fa-trash-can" onClick={() => {
                            setAudio(null);
                            setAddmode(true);
                        }}></i>
                    </div>}
                    {imglist?.map((img, i) => (
                        <div className='previewimg'>
                            <img src={img} alt="" key={`vimg${i}`} onClick={()=>imagePopup(img)} />
                            <i className="fa-solid fa-trash-can" onClick={() => deleteimg(img)}></i>
                        </div>
                    ))}
                    {addmode && !videolock && <input id="uploadbutton" accept={`image/*, ${!videolock && "video/*, audio/*"}`} onChange={e=>setFile(e.target.files)} type="file" />}
                    {addmode && videolock && <input id="uploadbutton" multiple accept="image/*" onChange={e=>setFile(e.target.files)} type="file" />}
                    {addmode && <label htmlFor="uploadbutton">
                        <div className='addwrapper'>
                            <i className="fa-solid fa-plus"></i>
                            {videolock && <span className='multiadd'>...</span>}
                        </div>
                    </label>}
                </div>
            </div>
        </div>
    )
}

export default CreateMoment;
