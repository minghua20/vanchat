import React, { useEffect } from "react";
// import Add from "../imgs/addAvatar.png"
import { createUserWithEmailAndPassword, updateProfile } from "firebase/auth";
import { auth, storage, db } from "../firebase";
import { useState } from "react";
import { ref, uploadBytesResumable, getDownloadURL } from "firebase/storage";
import { doc, setDoc } from "firebase/firestore"; 
import { Link, useNavigate } from "react-router-dom";
import { v4 as uuid } from "uuid";
import {RxAvatar} from "react-icons/rx";


const Register = () => {
    const [avatar, setAvatar] = useState(null);
    const [errMessage, setErrMessage] = useState('');
    const [preview, setPreview] = useState(null);
    const [isLoading, setIsLoading] = useState(false);
    const [isLoadingimg, setIsLoadingImg] = useState(false);
    const navigate = useNavigate();


    const getlink = () => {
        if (isLoading)
            return "";
        else
            return "/login";
    };

    const handleSubmit = async(e) => {
        setIsLoading(true);

        e.preventDefault()
        const displayName = e.target[0].value;
        const email = e.target[1].value;
        const password = e.target[2].value;
        // const file = e.target[3].files[0];

        if (preview === null) {
            setErrMessage("Please upload your avatar.");
            setIsLoading(false);
            return;
        }

        if (displayName === "") {
            setErrMessage("Please enter your username.");
            setIsLoading(false);
            return;
        }

        if (displayName.length > 16) {
            setErrMessage("Username exceeds maximum length.");
            setIsLoading(false);
            return;
        }

        try {
            const res = await createUserWithEmailAndPassword(auth, email, password);

            await updateProfile(res.user, {
                displayName,
                photoURL: preview,
            });
            // Add a new document in collection "users"
            await setDoc(doc(db, "users", res.user.uid), {
                uid: res.user.uid,
                displayName,
                email,
                photoURL: preview,
                friendlist: [],
            });

            // Add chats info in collection "userChats"
            await setDoc(doc(db, "userChats", res.user.uid), {});

            await setDoc(doc(db, "newfriendsApplication", res.user.uid), {});

            await setDoc(doc(db, "moments", res.user.uid), {
              momentuid: res.user.uid,
            });

            navigate("/");
            setIsLoading(false);

        } catch(err) {
            setIsLoading(false);

            if (err.code === 'auth/weak-password')
                setErrMessage('Password should be at least 6 characters');
            else if (err.code === 'auth/email-already-in-use')
                setErrMessage('Email is already in use');
            else if (err.code === 'auth/invalid-email')
                setErrMessage('Email is invalid');
            else
                setErrMessage(err.code);
        }
    };

    useEffect(() => {
        const uploadAvatar = () => {
            if (avatar !== null && avatar !== undefined) {
                setIsLoadingImg(true);

                const storageRef = ref(storage, uuid());
                const uploadTask = uploadBytesResumable(storageRef, avatar);
    
                // Update User Avater
                uploadTask.on(
                    "state_changed",
                    (snapshot) => {
                        const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                        switch (snapshot.state) {
                            case 'paused':
                            console.log('Upload is paused');
                            break;
                            case 'running':
                            // console.log('Upload is running');
                            break;
                            default:
                            break;
                        }
                        console.log("Upload is " + progress + "% done");
                    },
                    (error) => {
                        setErrMessage(error.message);
                    }, 
                    () => {
                        getDownloadURL(uploadTask.snapshot.ref).then(async (downloadURL) => {
                            setPreview(downloadURL);
                        });
                    }
                );
            }
        };
        uploadAvatar();
    }, [avatar]);


    return (
        <div className="formContainer">
            <div className="blob"></div>
            <div className="formWrapper">
                <span className="logo">VanChat</span>
                <span className="title">Register</span>
                <form onSubmit={handleSubmit}>
                    <input type="text" placeholder="User Name" />
                    <input type="email" placeholder="Email" />
                    <input type="password" placeholder="Password" />
                    <input accept="image/*" className="addAvatar" type="file" id="file" onChange={e=>setAvatar(e.target.files[0])} />
                    <label htmlFor="file">
                        {/* <img src={Add} alt="Avatar" /> */}
                        {/* <i className="avatar fa-solid fa-user-plus"></i> */}
                        {/* <i className="avatar fa-regular fa-circle-user"></i> */}
                        {(avatar === null) && <div className="avatarbox">
                            <i className="avatar"><RxAvatar /></i>
                        </div>}
                        {(avatar !== null) && <div className="avatarbox hide">
                            <img className="previewAvatar" src={preview} alt="" onLoad={() => setIsLoadingImg(false)} />
                        </div>}
                        <span>Add an avatar</span>
                        {isLoadingimg && <i className="fa fa-spinner fa-spin"></i>}
                        {(preview !== null) && !isLoadingimg && <i className="checkavatar fa-solid fa-check"></i>}
                    </label>
                    {errMessage && (<span className="errorMessage">{errMessage}</span>)}
                    <button disabled={isLoading}>Sign up{isLoading && <i className="fa fa-circle-o-notch fa-spin"></i>}</button>
                </form>
                <p>Already have an account? <Link to={getlink()}>Login here!</Link></p>
            </div>
        </div>
    )
}

export default Register;
