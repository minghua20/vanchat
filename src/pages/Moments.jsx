import { collection, doc, getDoc, onSnapshot, query, updateDoc, where } from 'firebase/firestore';
import React, { useState } from 'react'
import { useEffect } from 'react';
import { useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import Moment from '../components/Moment';
import { AuthContext } from '../context/AuthContext';
import { db, storage } from '../firebase';
import { v4 as uuid } from "uuid";
import { getDownloadURL, ref, uploadBytesResumable } from 'firebase/storage';


const Moments = () => {

    const {currentUser} = useContext(AuthContext);
    const [momentlist, setMomentlist] = useState([]);
    const [userchats, setUserchats] = useState([]);
    const [personalbg, setPersonalbg] = useState(null);
    const [updatebg, setUpdatebg] = useState(null);
    const navigate = useNavigate();


    const getfriendmoments = async () => {
        try {
            const res = await getDoc(doc(db, "users", currentUser.uid));
            let myfriendlist = (res.data()).friendlist;
            if (myfriendlist.filter(m => {return m === currentUser.uid}).length === 0)
                myfriendlist.push(currentUser.uid);

            const q = query(collection(db, "moments"), where("momentuid", "in", myfriendlist));
            onSnapshot(q, (doc) => {
                const resdata = doc.docs;
                if (resdata.length !== 0) {
                    let preMomentlist = [];
                    resdata.map((r) => {
                        let addMoments =  Object.entries(r.data());
                        addMoments = addMoments.filter(m => {return m[0] !== "momentuid"});
                        preMomentlist = preMomentlist.concat(addMoments);
                        setMomentlist(preMomentlist);
                        return 1;
                    })
                }
            });
        } catch(err) {
        //   console.log(err);
        }
    }
    
    
    useEffect(() => {
        if (currentUser) {
            if(currentUser.uid !== undefined)
                getfriendmoments();
        }
    
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [currentUser.uid]);

    useEffect(() => {
        const getuserchats = async () => {
            const res = await getDoc(doc(db, "userChats", currentUser.uid));
            setUserchats(Object.entries(res.data()));
        };
        currentUser.uid && getuserchats();
    }, [momentlist, currentUser.uid]);

    useEffect(() => {
        const getpersonalBg = async () => {
            const res = await getDoc(doc(db, "users", currentUser.uid));
            const resdata = res.data();
            setPersonalbg(resdata.personalbg);
        };
        currentUser.uid && getpersonalBg();
    }, [currentUser.uid, personalbg]);

    useEffect(() => {
        const uploadbg = async () => {
            if (updatebg !== null && updatebg !== undefined) {
                const storageRef = ref(storage, uuid());
                const uploadTask = uploadBytesResumable(storageRef, updatebg);

                uploadTask.on(
                    "state_changed",
                    (snapshot) => {
                        const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                        switch (snapshot.state) {
                            case 'paused':
                            console.log('Upload is paused');
                            break;
                            case 'running':
                            // console.log('Upload is running');
                            break;
                            default:
                            break;
                        }
                        console.log("Upload is " + progress + "% done");
                    },
                    (error) => {
                        // console.log(error.message);
                    },
                    () => {
                        getDownloadURL(uploadTask.snapshot.ref).then(async (downloadURL) => {
                            await updateDoc(doc(db, "users", currentUser.uid), {
                                personalbg: downloadURL,
                            });
                            setPersonalbg(downloadURL);
                            setUpdatebg(null);
                        });
                    }
                );
            }
        };
        currentUser.uid && uploadbg();
    }, [updatebg, currentUser.uid]);

    const defaultbgimg = document.getElementById("defaultbgimg");
    const bgimg = document.getElementById("bgimg");
    const momenthead = document.getElementById("momenthead");

    const handleScroll = (e) => {
        if (personalbg) {
            if (e.target.scrollTop >= bgimg.clientHeight) {
                momenthead.classList.add("showbar");
            }
            else
                momenthead.classList.remove("showbar");
        }
        else {
            if (e.target.scrollTop >= defaultbgimg.clientHeight) {
                momenthead.classList.add("showbar");
            }
            else
                momenthead.classList.remove("showbar");
        }
    };


    return (
        <div className="Moments">
            <div className="Momentcontainer">
                <div className="Momentheader" id='momenthead'>
                    <i onClick={()=>navigate("/")} className="return fa-solid fa-chevron-left"></i>
                    <p>Moments</p>
                    <i onClick={()=>navigate("/createMoment")} className="createmoment fa-solid fa-camera-retro"></i>
                </div>
                <div className="MomentInfo" onScroll={handleScroll}>
                    <div className='personalbg'>
                        {personalbg && <img id='bgimg' src={personalbg} alt='' />}
                        {(personalbg === undefined) && <div className='defaultbg' id='defaultbgimg'>
                            <div className='vanchatlogo'>
                                <i className="fa-solid fa-v"></i>
                                <i className="fa-solid fa-a"></i>
                                <i className="fa-solid fa-n"></i>
                                <i className="fa-solid fa-c"></i>
                                <i className="fa-solid fa-h"></i>
                                <i className="fa-solid fa-a"></i>
                                <i className="fa-solid fa-t"></i>
                            </div>
                            <span>{"< Upload your personal cover from the bottom left >"}</span>
                        </div>}
                        <div className='userinfo'>
                            <span>{currentUser.displayName}</span>
                            <img src={currentUser.photoURL} alt="" />
                        </div>
                        <div className='changecover'>
                            <input type="file" accept='image/*' id='coverupload' onChange={e => setUpdatebg(e.target.files[0])} />
                            <label htmlFor="coverupload">
                                <i className="fa-regular fa-images"></i>
                                <span>Change Cover</span>
                            </label>
                        </div>
                    </div>
                    {momentlist?.sort((a, b) => b[1].date - a[1].date).map((moment, index) => (
                        <Moment moment={moment} index={index} userchats={userchats} key={moment[0]} />
                    ))}
                </div>
            </div>
        </div>
    )
}

export default Moments;
