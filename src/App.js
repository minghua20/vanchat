import Home from "./pages/Home";
import Login from "./pages/Login";
import Register from "./pages/Register";
import "./style.scss"
import { Routes, Route, Navigate, HashRouter } from "react-router-dom";
import { useContext } from "react";
import { AuthContext } from "./context/AuthContext";
import Moments from "./pages/Moments";
import CreateMoment from "./pages/CreateMoment";


function App() {
  
  const {currentUser} = useContext(AuthContext);
  const ProtectedRoute = ({children}) => {
    if(!currentUser) {
      return (
        <Navigate to="/login" />
      )
    }
    else {
      return children
    }
  };


  return (
    <HashRouter>
    {/* <BrowserRouter> */}
      <Routes>
        <Route path="/">
          <Route index element={
            <ProtectedRoute>
              <Home />
            </ProtectedRoute>
          } />
          <Route path="login" element={<Login />} />
          <Route path="register" element={<Register />} />
          <Route path="moments" element={<ProtectedRoute><Moments /></ProtectedRoute>}></Route>
          <Route path="createMoment" element={<ProtectedRoute><CreateMoment /></ProtectedRoute>}></Route>
        </Route>
      </Routes>
    {/* </BrowserRouter> */}
    </HashRouter>
  );
}

export default App;
