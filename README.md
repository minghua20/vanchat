# [VanChat](https://minghua20.gitlab.io/vanchat/)

## Quick Start

In the project directory, you can run the app in the development mode:

```
npm install
npm start
```

Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

## Login
![login_page](figures/login.jpg)
Enter your email and password to login, if you do not have an account, you can click the register link to turn to register page.

## Register
![register_page](figures/register.jpg)
Enter your username, email and password then upload an avatar to register an account.

## Home Page
After you login, you can see the home page.
![home_page](figures/home.jpg)

### Add new friends

In this page, you can first search a username or email of your friends' account, then click the note icon to add some remarks, then click the add icon to send your friend application.
![addfriend1](figures/friend1.jpg)

After your friend accepts your application, you can start chatting!
![addfrined2](figures/friend2.jpg)

![addfriend3](figures/friend3.jpg)

### Update information
If you want to change your username or avatar, you can click the icon next to your personal information, then you can change your name or upload a new avatar.
![updateinfo](figures/updateinfo.jpg)

You can also click the "ellipsis" icon at the top right corner to add an alias for your friend.
![alias](figures/alias.jpg)

### Send more than plain text

Besides, you can click the file icon to send files like images, videos and audios. If you want to send some emojis, just click the smile face icon!
![various_input](figures/chat1.jpg)

![files_emoji](figures/chat2.jpg)

There is a microphone icon above the "Send" button, you can click it to send an audio message.
![audio_message1](figures/mic1.jpg)
![audio_message2](figures/mic2.jpg)


## Moment
"Moment" is a place where users can share the interesting moments in their lives to other users who are in the friend list. It's more convenient to share something "private" with your friends or relatives! You can click the icon on the right of the search bar to enter the Moment page.
![moment_icon](figures/moment_icon.jpg)

![moment_page](figures/moment_page.jpg)

For new user, you can click "Change Cover" to upload an image as your personal moment cover.
![moment_bg](figures/moment1.jpg)

### Post a moment
If you want to make a post, you can click the camera icon at the top right corner. Then, you can type your thoughts, add emojis if you want and click the plus icon to upload the images, videos or audios you want to share.
![post_moment](figures/post.jpg)

### Like and comment
For each post, you can click "Like" or click "Comment" to leave some comments to this post.
![like_comment](figures/like_comment.jpg)

You can reply to someone's comment by clicking his/her username.
![comment](figures/comment.jpg)

##
This Web Application uses Firebase as backend, for users in China Mainland, you need to connect to VPN to get access to VanChat.
